package com.telstra.ordersystem.itemservice.ItemService.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.telstra.ordersystem.itemservice.ItemService.exception.ItemNotFoundException;
import com.telstra.ordersystem.itemservice.ItemService.itemrepository.ItemRepository;
import com.telstra.ordersystem.itemservice.ItemService.model.Item;

@Service
public class ItemService {
	
	@Autowired
	ItemRepository itemRepository;
	
	@Autowired
	private Environment environment;
	
	public Integer addItem(Item item) {
		
		Integer itemId = itemRepository.saveAndFlush(item).getItemId();
		return itemId;

	}
	public Item getItem(Integer itemId) throws ItemNotFoundException {
		
		Item item = itemRepository.findOne(itemId);
		if(item==null) {
			throw ItemNotFoundException.getInstance(environment.getProperty("ERCODE.ItemNotFound"), environment.getProperty("ERMSG.ItemNotFound"));
		}
		return item;

	}

}
