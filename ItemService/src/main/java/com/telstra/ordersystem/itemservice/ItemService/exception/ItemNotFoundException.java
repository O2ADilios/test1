package com.telstra.ordersystem.itemservice.ItemService.exception;

public class ItemNotFoundException extends Exception{
	
	private String code;
	private String message;
	
	public static ItemNotFoundException getInstance(String iErrorCode, String strMsg){
		return new ItemNotFoundException(iErrorCode,strMsg);
	}
	
	public ItemNotFoundException(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
