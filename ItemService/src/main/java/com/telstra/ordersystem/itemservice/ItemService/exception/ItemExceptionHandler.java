package com.telstra.ordersystem.itemservice.ItemService.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ItemExceptionHandler {
	
	@ExceptionHandler({ItemNotFoundException.class})
	public ResponseEntity<ErrorResource>  handleUNFInternalException( ItemNotFoundException exception) {


		ErrorResource exceptionResponse = new ErrorResource();
		exceptionResponse.setCode(exception.getCode());
		exceptionResponse.setMessage(exception.getMessage());
		return  new ResponseEntity<ErrorResource>(exceptionResponse, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler({Exception.class})
	public ResponseEntity<ErrorResource>  handleInternalException(Exception exception) {


		ErrorResource exceptionResponse = new ErrorResource();
		exceptionResponse.setMessage(exception.getMessage());
		return  new ResponseEntity<ErrorResource>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
